from MasterMind import Master_Mind


def test_mastermind():

    m = Master_Mind()
    guess=55500
    m.answer = 10000
    guess_output = m.check(guess)
    expected_output = {'high_low':'High','is_correct': False, 'cows': 2, 'bulls': 2}

    assert guess_output == expected_output
    guess =10000
    guess_output = m.check(guess)
    expected_output = {'high_low':'Same','is_correct': True, 'cows': 5, 'bulls': 5}

    assert guess_output == expected_output


    print("All test case passed")
    m.reset()

    

test_mastermind()
