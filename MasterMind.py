import random

class Master_Mind:
    def __init__(self):
        pass

    def generate_Answer(self):
        return random.randint(10000,100000)

    def user_input(self):
        try:
            user_guess=input("Guess your number:")
            return int(user_guess)
        except ValueError:
            print('You have not entered a valid number')
            return self.user_input()



    def check(self, guess):
        answer = self.answer
        #print(guess,answer)
        status=False
        _cows=0
        _bulls=0
        position='Low'
        #Checking weather input is higher or lower
        if guess>answer:
            position='High'
        #Converting "guessed_answer" & "correct_answer" to list
        guess_list=self.number_To_List(guess)
        answer_list=self.number_To_List(answer)
        #print("guess list:", guess_list)
        #print("answer list:", answer_list)
        #Checking for Cows
        for i in guess_list:
            if i in answer_list:
                _cows+=1
                
        #Checking for Bulls
        for i in range(5):
                try:
                    if guess_list[i]==answer_list[i]:
                        _bulls+=1
                except IndexError:
                    print('Enter a 5 digit Number')
                    break
        #print('cows:',_cows)
        #print('bulls:',_bulls)
        if(guess==answer):
            status=True
            position='Same'
        #print(status)  
        #print(position)    
        return {'high_low':position, 'is_correct': status, 'cows':_cows, 'bulls':_bulls}
    
    def controller(self):
        user_choice=input("Enter 1 to play \nEnter 2 for instructions\n")
        if int(user_choice) is 1:
            self.answer=self.generate_Answer()
            answer = self.answer
            guess_count=5
            while guess_count >=0:
                if guess_count==0:
                    print('Correct answer was:',answer,'\nThanks for trying')
                    if self.reset():
                        self.controller()
                    else:
                        break
                guess=self.user_input()
                #printing the status
                print('Your guess is:',self.check(guess)['high_low'],'||Cows--->',self.check(guess)['cows'],'||Bulls--->',self.check(guess)['bulls'])
                #Break condition when correct answer
                if self.check(guess)['is_correct']:
                    print("Yayyyy!! You have a MasterMind")
                    break
                guess_count-=1
        elif int(user_choice) is 2:
            print("INSTRUCTIONS")
        else:
            print("Enter a valid input")
    def reset(self):
        user_choice=input("Enter 1 to play again \nEnter 2 to exit\n")
        status=False
        if int(user_choice) is 1:
            status=True
        elif int(user_choice) is 2:
            status=False
            print("Thanks for playing!!\n==========================\nHAVE A GOOD DAY")
        return status
        
    

    def number_To_List(self, ans):
        ansToList=[]
        temp=ans
        while temp != 0:
            ansToList.append(temp%10)
            temp=int(temp/10)
        ansToList.reverse()
        return ansToList


#obj=Master_Mind()


if __name__ == "__main__":
    m = Master_Mind()
    m.controller()