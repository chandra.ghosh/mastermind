import random
class MaxAttemptExceeded(Exception):
    print('Exceeded MAX Attempts')
    pass
class MasterMind:
    def __init__(self, game_digits=5, max_attempts=3):
        self.game_digits=game_digits
        self.max_attempts=max_attempts

    def start(self):
        self.answer=random.randint(
            10**(self.game_digits-1),
            (10**self.game_digits)
        )
        self.attempts=0
       

    def guess(self, guessed_number):
               
        self.attempts+=1
        position=''
        if guessed_number > self.answer:
            position='High'
        else:
            position='Low'

        str_guessed_num=str(guessed_number)
        print(str_guessed_num)
        if len(str_guessed_num)!= self.game_digits:
            raise ValueError("You have not entered a", self.game_digits, "digit number")
        
        str_answer=str(self.answer)
        is_correct = self.is_guess_correct(str_answer, str_guessed_num)
        if is_correct:
            return{
                'position':position,
                'is_correct': True,
                'cows':self.game_digits,
                'bulls':self.game_digits,
                'attempts':self.attempts,
                'max_attempts':self.max_attempts
            }
        cows = self.cows(int(str_answer), guessed_number)
        bulls= self.bulls(int(str_answer), guessed_number)
        return{
            'position':position,
            'is_correct': False,
            'cows':cows,
            'bulls':bulls,
            'attempts':self.attempts,
            'max_attempts':self.max_attempts
        }


    def is_guess_correct(self, str_answer, str_guessed_num):
        return str_answer == str_guessed_num

    def cows(self, answer, guessed_num):
        #print('cows', answer, guessed_num)
        _cows=0
        guess_list=self.number_To_List(guessed_num)
        answer_list=self.number_To_List(answer)
        for i in guess_list:
            if i in answer_list:
                _cows+=1

        return _cows
    def bulls(self, answer, guessed_num):
        _bulls=0
        guess_list=self.number_To_List(guessed_num)
        answer_list=self.number_To_List(answer)
        for i in range(self.game_digits):
            if guess_list[i]==answer_list[i]:
                _bulls+=1
        return _bulls

    def number_To_List(self, ans):
        ansToList=[]
        temp=ans
        while temp != 0:
            ansToList.append(temp%10)
            temp=int(temp/10)
        ansToList.reverse()
        return ansToList
    def game_controller(self):
        user_choice=input("Enter 1 to play \nEnter 2 for instructions\n")
        if int(user_choice) is 1:
            self.game_digits=int(input('For how many digits you want to play?'))
            self.max_attempts=int(input('In how many attempts would you like being called a MASTERMIND?'))
            self.start()
        
        while self.attempts<=self.max_attempts:
            if self.attempts == self.max_attempts:
                print('\n \nOH!! CRAP**** \n \n Correct answer was===>', self.answer, '\nThanks for playing')
                replay_game=int(input('Do you want to play again???\n1. Yes>\n2. No>'))
                if replay_game is 1:
                    self.start()
                else:
                    print('EXITING MasterMind')
                    break


            guessed_number=int(input('Enter your guess====>'))
            status=self.guess(guessed_number)
            

            print('Your guess is===>',status['position'],'||COWS===>',status['cows'], '||BULLS===>',status['bulls'])
            
    
obj=MasterMind()

obj.game_controller()

    
        
    